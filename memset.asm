.186
.model tiny
.code
public memset
locals @@

;==============================================================================
; memset -> copies AX to the first CX characters of ES:[DI]
; In: AX - char to set
; 	  CX - str len
; 	  ES:[DI] - str to be set
;==============================================================================

memset proc
		cld
		rep stosw	

		ret
		endp
end
