.186
.model tiny
.code
public memcmp 
locals @@

;==============================================================================
; memcmp -> compares first CX bytes of DS:[SI] & ES:[DI]
; In:	CX - len < 2^16
; 		DS:[SI], ES:[DI] - ptrs to the strings
; Out:	DX - -1 if <;
; 			  0 if =;
;			  1 if >;
;==============================================================================

memcmp proc
		cld 	    ;frwd dir
		repe cmpsb 
		
		je @@eq
		ja @@grt
		jb @@les

@@eq:
		mov dx, 0
		jmp @@exit

@@grt:
		mov dx, 1
		jmp @@exit

@@les:
		mov dx, -1

@@exit:
		ret
endp

end
