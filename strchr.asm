.186
.model tiny
.code
public strchr
extrn strlen: proc
extrn memchr: proc
locals @@

;==============================================================================
; strchr -> index of first occurence of char in 0-tmd str or -1 if not found
; In: ES:[DI] - ptr to the string
; 	  AL - char to find
; Out: DX - index of first occurence
; Destroy: CX;
;==============================================================================

strchr proc
		call strlen ;dx = strlen
		mov cx, dx  ;cx = strlen

		call memchr ;dx = i
		
		ret
		endp
end
