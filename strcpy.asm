.186
.model tiny
.code
public strcpy
extrn strlen: proc
extrn memcpy: proc
locals @@

;==============================================================================
; strcpy -> copies DS:[SI] to ES:[DI]
; In: DS:[SI] - 0-trmd source
; 	  ES:[DI] - destination
; Destroy: CX, DX
;==============================================================================

strcpy proc
		call strlen ;dx = strlen
		mov cx, dx  ;cx = strlen
			
		call memcpy

		ret
		endp
end
