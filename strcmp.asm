.186
.model tiny
.code
public strcmp
extrn strlen: proc
extrn memcmp: proc
locals @@

;==============================================================================
; strcmp -> compares DS:[SI] & ES:[DI]
; In: DS:[SI], ES[DI] - ptrs to the strings
; Out: DX - -1 if <
;			 0 if =
;			 1 if >
;==============================================================================

strcmp proc
	call strlen ;dx = strlen
	mov cx, dx  ;cx = strlen

	call memcmp

	ret
	endp
end
