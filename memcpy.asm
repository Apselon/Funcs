.186
.model tiny
.code
public memcpy
locals @@

;==============================================================================
; memcpy -> copies CX bytes from DS:[SI] to ES:[DI]
; In: CX - str len
; 	  DS:[SI] - source
; 	  ES:[DI] - destination
;==============================================================================

memcpy proc
		cld
	
		rep movsb 

@@exit:
		ret
		endp
end
